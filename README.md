# NETLIFY HTML TESTER

[![Netlify Status](https://api.netlify.com/api/v1/badges/2aabb78f-6694-489c-ad01-21cf4a63056b/deploy-status)](https://app.netlify.com/sites/wonderful-meitner-1119a8/deploys)

## A Basic Way To Deploy A Basic Website To Netlify

3 Reasons Why I Made This

- To Get Practice In HTML
- To See If Netlify Works With HTML
- To Work With Bitbucket and It's Git Services

### [Test Out The Site](https://wonderful-meitner-1119a8.netlify.app/)

### Hope It Works 😀